﻿using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Infrastructure.Identity;
using IWG.Heimdall.Infrastructure.Persistence.DBContexts;
using IWG.Heimdall.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IWG.Heimdall.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration, string migrationAssemblyName)
    {

        string provider = configuration.GetValue<string>("Database:Provider");


        services.AddDbContext<ApplicationDbContext>(
                    options => _ = provider switch
                    {
                        "SqlServer" => options.UseSqlServer(
                                        configuration.GetValue<string>("Database:DefaultConnection"),
                                        b => b.MigrationsAssembly(migrationAssemblyName)),

                        "PostGreSql" => options.UseNpgsql(
                                          configuration.GetValue<string>("Database:DefaultConnection"),
                                     b => b.MigrationsAssembly(migrationAssemblyName)),

                        "InMemory" => options.UseInMemoryDatabase("IWG.HeimdallDb"),

                        _ => throw new Exception($"Unsupported provider: {provider}")
                    });


        services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<ApplicationDbContext>());

        services.AddScoped<IDomainEventService, DomainEventService>();

        services
            .AddDefaultIdentity<ApplicationUser>()
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>();

        services.AddIdentityServer()
            .AddApiAuthorization<ApplicationUser, ApplicationDbContext>();

        services.AddTransient<IDateTime, DateTimeService>();
        services.AddTransient<IIdentityService, IdentityService>();

        services.AddAuthentication()
            .AddIdentityServerJwt();

        services.AddAuthorization(options =>
            options.AddPolicy("CanPurge", policy => policy.RequireRole("Administrator")));

        return services;
    }
}
