﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IWG.Heimdall.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IWG.Heimdall.Infrastructure.Persistence.Configurations;

public class WorkplaceConfiguration : IEntityTypeConfiguration<Workplace>
{
    public void Configure(EntityTypeBuilder<Workplace> builder)
    {
       
        builder.Ignore(e => e.DomainEvents);

        builder.Property(t => t.CentreName)
            .HasMaxLength(200)
            .IsRequired();

        builder.Property(t => t.Address1)
            .HasMaxLength(100)
            .IsRequired();

    }

}
