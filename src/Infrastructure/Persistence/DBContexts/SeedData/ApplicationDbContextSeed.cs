﻿using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.Domain.ValueObjects;
using IWG.Heimdall.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;

namespace IWG.Heimdall.Infrastructure.Persistence.DBContexts.SeedData;

public static class ApplicationDbContextSeed
{
    public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
    {
        var administratorRole = new IdentityRole("Administrator");

        if (roleManager.Roles.All(r => r.Name != administratorRole.Name))
        {
            await roleManager.CreateAsync(administratorRole);
        }

        var administrator = new ApplicationUser { UserName = "administrator@localhost", Email = "administrator@localhost" };

        if (userManager.Users.All(u => u.UserName != administrator.UserName))
        {
            await userManager.CreateAsync(administrator, "Administrator1!");
            await userManager.AddToRolesAsync(administrator, new[] { administratorRole.Name });
        }
    }

    public static async Task SeedSampleDataAsync(ApplicationDbContext context)
    {
        // Seed, if necessary
        /**
         * 1	812	BRUSSELS, Louise Centre	Stephanie Square Centre	Avenue Louise 65	Box 11	Brussels		1050	Belgium
3	716	LONDON, Trafalgar Square	1 Northumberland Avenue Trafalgar Square	NULL	NULL	London	Greater London	WC2N 5BW	United Kingdom
5	559	Paris, Signature, 72 Faubourg St Honoré	72 rue du Faubourg Saint Honoré	NULL	NULL	Paris	Paris	75008	France
11	866	BEIJING, Lufthansa Center	Beijing Lufthansa Center C203	50 Liangmaqiao Rd	Chaoyang District	Beijing		100016	China
15	681	POTTERS BAR, High Street	Maple House	High Street	NULL	Potters Bar	Hertfordshire	EN6 5BS	United Kingdom
21	420	LISBON, Avenida da Liberdade	Avenida da Liberdade, 110	NULL	NULL	Lisbon		1269-046	Portugal
22	939	VIENNA, Mariahilfer Strasse	Mariahilfer Straße 123/3	NULL	NULL	Vienna		1060	Austria
24	466	ROISSY HQ, Airport	Le Dôme	1, rue de la Haye	BP 12910	Tremblay en France		93290	France
34	897	ROTTERDAM, City Centre	Weena 290, 10th floor	NULL	NULL	Rotterdam		3012 NJ	Netherlands
42	510	JOHANNESBURG, Sandton Nelson Mandela Square	West Tower, 2nd Floor, Nelson Mandela Square	Maude Street	Sandown	Johannesburg		2146	South Africa
47	986	Amsterdam, Atrium	Strawinskylaan 3051	Atrium Building 4th Floor	NULL	Amsterdam		1077 ZX	Netherlands
48	1006	PRAGUE City Center	Klimentska 46	NULL	NULL	Prague		110 02	Czech Republic
*/
        if (!context.Workplaces.Any())
        {

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 1,
                WorkplaceExternalIdentifier = 812,
                CentreName = "BRUSSELS, Louise Centre",
                Address1 = "Stephanie Square Centre",
                Address2 = "Avenue Louise 65",
                Address3 = "Box 11",
                City = "Brussels",
                Zip = "1050",
                State = "",
                CountryID = 1
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 3,
                WorkplaceExternalIdentifier = 716,
                CentreName = "LONDON, Trafalgar Square",
                Address1 = "1 Northumberland Avenue Trafalgar Square",
                Address2 = "",
                Address3 = "",
                City = "London",
                Zip = "WC2N 5BW",
                State = "Greater London",
                CountryID = 2
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 5,
                WorkplaceExternalIdentifier = 559,
                CentreName = "Paris, Signature, 72 Faubourg St Honoré",
                Address1 = "72 rue du Faubourg Saint Honoré",
                Address2 = "",
                Address3 = "",
                City = "Paris",
                Zip = "75008",
                State = "Paris",
                CountryID = 3
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 11,
                WorkplaceExternalIdentifier = 866,
                CentreName = "BEIJING, Lufthansa Center",
                Address1 = "Beijing Lufthansa Center C203",
                Address2 = "50 Liangmaqiao Rd",
                Address3 = "Chaoyang District",
                City = "Beijing",
                Zip = "100016",
                State = "",
                CountryID = 4
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 15,
                WorkplaceExternalIdentifier = 681,
                CentreName = "POTTERS BAR, High Street",
                Address1 = "Maple House",
                Address2 = "High Street",
                Address3 = "",
                City = "Potters Bar",
                Zip = "EN6 5BS",
                State = "Hertfordshire",
                CountryID = 2
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 21,
                WorkplaceExternalIdentifier = 420,
                CentreName = "LISBON, Avenida da Liberdade",
                Address1 = "Avenida da Liberdade, 110",
                Address2 = "",
                Address3 = "",
                City = "Lisbon",
                Zip = "1269-046",
                State = "",
                CountryID = 5
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 22,
                WorkplaceExternalIdentifier = 939,
                CentreName = "VIENNA, Mariahilfer Strasse",
                Address1 = "Mariahilfer Straße 123/3",
                Address2 = "",
                Address3 = "",
                City = "Vienna",
                Zip = "1060",
                State = "",
                CountryID = 6
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 24,
                WorkplaceExternalIdentifier = 466,
                CentreName = "ROISSY HQ, Airport",
                Address1 = "Le Dôme",
                Address2 = "1, rue de la Haye",
                Address3 = "BP 12910",
                City = "Tremblay en France",
                Zip = "93290",
                State = "",
                CountryID = 3
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 34,
                WorkplaceExternalIdentifier = 466,
                CentreName = "ROTTERDAM, City Centre",
                Address1 = "Weena 290, 10th floor",
                Address2 = "",
                Address3 = "",
                City = "Rotterdam",
                Zip = "3012 NJ",
                State = "",
                CountryID = 7
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 42,
                WorkplaceExternalIdentifier = 510,
                CentreName = "JOHANNESBURG, Sandton Nelson Mandela Square",
                Address1 = "West Tower, 2nd Floor, Nelson Mandela Square",
                Address2 = "Maude Street",
                Address3 = "Sandown",
                City = "Johannesburg",
                Zip = "2146",
                State = "",
                CountryID = 8
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 47,
                WorkplaceExternalIdentifier = 986,
                CentreName = "Amsterdam, Atrium",
                Address1 = "Strawinskylaan 3051",
                Address2 = "Atrium Building 4th Floor",
                Address3 = "",
                City = "Amsterdam",
                Zip = "1077 ZX",
                State = "",
                CountryID = 7
            });

            context.Workplaces.Add(new Workplace
            {
                //WorkplaceIdentifier = 48,
                WorkplaceExternalIdentifier = 1006,
                CentreName = "PRAGUE City Center",
                Address1 = "Klimentska 46",
                Address2 = "",
                Address3 = "",
                City = "Prague",
                Zip = "110 02",
                State = "",
                CountryID = 9
            });

            await context.SaveChangesAsync();
        }
    }
}
