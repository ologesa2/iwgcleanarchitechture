﻿using IWG.Heimdall.Application.Common.Interfaces;

namespace IWG.Heimdall.Infrastructure.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.UtcNow;
}
