﻿using IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;
using Microsoft.AspNetCore.Mvc;

namespace IWG.Heimdall.WebUI.Controllers;

public class WeatherForecastController : ApiControllerBase
{
    [HttpGet]
    public async Task<IEnumerable<Contracts.WeatherForecast>> Get()
    {
        var client = MassTransitMediator.CreateRequestClient<IGetWeatherForecastRequest>();
        var response = await client.GetResponse<IGetWeatherForecastResponse>(new object { });

        return response.Message.Temperatures.Select(x => new Contracts.WeatherForecast(x.Day, x.Temperature));
    }
}
