﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IWG.Heimdall.WebUI.Controllers;

[ApiController]
[Route("api/[controller]")]
public abstract class ApiControllerBase : ControllerBase
{
    private MassTransit.Mediator.IMediator _mtMediator = null!;
    private MediatR.IMediator _mediatrMediator = null!;


    protected MassTransit.Mediator.IMediator MassTransitMediator => _mtMediator ??= HttpContext.RequestServices.GetRequiredService<MassTransit.Mediator.IMediator>();
    protected MediatR.IMediator MediatRMediator => _mediatrMediator ??= HttpContext.RequestServices.GetRequiredService<MediatR.IMediator>();
}
