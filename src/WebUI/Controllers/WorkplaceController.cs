﻿using IWG.Heimdall.Application.Common.Models;
using IWG.Heimdall.Application.Workplaces.Commands.CreateWorkplace;
using IWG.Heimdall.Application.Workplaces.Commands.DeleteWorkplace;
using IWG.Heimdall.Application.Workplaces.Commands.UpdateWorkplace;
using IWG.Heimdall.Application.Workplaces.Commands.UpdateWorkplaceDetail;
using IWG.Heimdall.Application.Workplaces.Queries.GetWorkplaceByIdQuery;
using IWG.Heimdall.Application.Workplaces.Queries.GetWorkplacesWithPagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IWG.Heimdall.WebUI.Controllers;

//[Authorize]
public class WorkplaceController : ApiControllerBase
{
    [HttpGet]
    public async Task<ActionResult<PaginatedList<WorkplaceBriefDto>>> GetWorkplacesWithPagination([FromQuery] GetWorkplacesWithPaginationQuery query)
    {
        return await MediatRMediator.Send(query);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<WorkplaceBriefDto>> GetWorkplaceById(int id)
    {
        return await MediatRMediator.Send(new GetWorkplaceByIdQuery{ WorkplaceIdentifier =id});
    }

    [HttpPost]
    public async Task<ActionResult<int>> Create(CreateWorkplaceCommand command)
    {
        return await MediatRMediator.Send(command);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult> Update(int id, UpdateWorkplaceCommand command)
    {
        command.SetWorkplaceIdentifier(id);

        await MediatRMediator.Send(command);

        return NoContent();
    }

    [HttpPut("[action]")]
    public async Task<ActionResult> UpdateItemDetails(int id, UpdateWorkplaceDetailCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await MediatRMediator.Send(command);

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        await MediatRMediator.Send(new DeleteWorkplaceCommand { WorkplaceIdentifier = id });

        return NoContent();
    }
}
