﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.WebUI.Pages.ContextWrapper;
using IWG.Heimdall.Infrastructure.Persistence.DBContexts;

namespace IWG.Heimdall.WebUI.Pages.WorkPlace
{
    public class DeleteModel : PageModel
    {
        //private readonly IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext _context;

        //public DeleteModel(IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext context)
        //{
        //    _context = context;
        //}


        private readonly ApplicationDbContext _context;

        public DeleteModel(ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Workplace Workplace { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Workplace = await _context.Workplaces.FirstOrDefaultAsync(m => m.WorkplaceIdentifier == id);

            if (Workplace == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Workplace = await _context.Workplaces.FindAsync(id);

            if (Workplace != null)
            {
                _context.Workplaces.Remove(Workplace);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
