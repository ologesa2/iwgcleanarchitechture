﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.WebUI.Pages.ContextWrapper;
using IWG.Heimdall.Infrastructure.Persistence.DBContexts;

namespace IWG.Heimdall.WebUI.Pages.WorkPlace
{
    public class EditModel : PageModel
    {
        //private readonly IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext _context;

        //public EditModel(IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext context)
        //{
        //    _context = context;
        //}

        private readonly ApplicationDbContext _context;

        public EditModel(ApplicationDbContext context)
        {
            _context = context;
        }


        [BindProperty]
        public Workplace Workplace { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Workplace = await _context.Workplaces.FirstOrDefaultAsync(m => m.WorkplaceIdentifier == id);

            if (Workplace == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Workplace).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkplaceExists(Workplace.WorkplaceIdentifier))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool WorkplaceExists(int id)
        {
            return _context.Workplaces.Any(e => e.WorkplaceIdentifier == id);
        }
    }
}
