﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.WebUI.Pages.ContextWrapper;
using IWG.Heimdall.Infrastructure.Persistence.DBContexts;

namespace IWG.Heimdall.WebUI.Pages.WorkPlace
{
    public class CreateModel : PageModel
    {
        //private readonly IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext _context;

        //public CreateModel(IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext context)
        //{
        //    _context = context;
        //}


        private readonly ApplicationDbContext _context;

        public CreateModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Workplace Workplace { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Workplaces.Add(Workplace);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
