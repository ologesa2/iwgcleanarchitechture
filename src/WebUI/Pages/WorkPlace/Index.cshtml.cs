﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.WebUI.Pages.ContextWrapper;
using IWG.Heimdall.Infrastructure.Persistence.DBContexts;

namespace IWG.Heimdall.WebUI.Pages.WorkPlace
{
    public class IndexModel : PageModel
    {
        //private readonly IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext _context;

        //public IndexModel(IWG.Heimdall.WebUI.Pages.ContextWrapper.PageDbContext context)
        //{
        //    _context = context;
        //}

        private readonly ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext context)
        {
            _context = context;
        }


        public IList<Workplace> Workplace { get;set; }

        public async Task OnGetAsync()
        {
            Workplace = await _context.Workplaces.ToListAsync();
        }
    }
}
