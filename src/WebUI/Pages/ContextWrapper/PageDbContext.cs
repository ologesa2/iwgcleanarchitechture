﻿using Duende.IdentityServer.EntityFramework.Options;
using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Infrastructure.Persistence.DBContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Options;

namespace IWG.Heimdall.WebUI.Pages.ContextWrapper;

public class PageDbContext : ApplicationDbContext, IDesignTimeDbContextFactory<PageDbContext>
{
    public PageDbContext(DbContextOptions<ApplicationDbContext> options, IOptions<OperationalStoreOptions> operationalStoreOptions, ICurrentUserService currentUserService, IDomainEventService domainEventService, IDateTime dateTime) :
        base(options, operationalStoreOptions, currentUserService, domainEventService, dateTime)
    {
    }

    public PageDbContext() : base(new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase("IWG.HeimdallDb").Options, new OperationalStoreOptionsMigrations(), null, null, null)
    {

    }


    public PageDbContext CreateDbContext(string[] args)
    {
        return new PageDbContext();
    }
}

internal class OperationalStoreOptionsMigrations : IOptions<OperationalStoreOptions>
{
    public OperationalStoreOptions Value => new OperationalStoreOptions()
    {
        DeviceFlowCodes = new TableConfiguration("DeviceCodes"),
        EnableTokenCleanup = false,
        PersistedGrants = new TableConfiguration("PersistedGrants"),
        TokenCleanupBatchSize = 100,
        TokenCleanupInterval = 3600,
    };

}

