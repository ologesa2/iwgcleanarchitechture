﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWG.Heimdall.Domain.Entities;

public class Workplace : AuditableEntity, IHasDomainEvent
{
    [Key]
    public int WorkplaceIdentifier { get; set; }
    public int WorkplaceExternalIdentifier { get; set; }
    public string CentreName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public int CountryID { get; set; }

    public List<DomainEvent> DomainEvents  { get; set; } = new List<DomainEvent>();
}
