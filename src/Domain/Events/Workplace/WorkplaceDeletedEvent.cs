﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWG.Heimdall.Domain.Events.Workplace;

public class WorkplaceDeletedEvent : DomainEvent
{
    public WorkplaceDeletedEvent(Entities.Workplace workplace)
    {
        Workplace = workplace;
    }

    public Entities.Workplace Workplace { get; }
}
