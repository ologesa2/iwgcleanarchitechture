﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWG.Heimdall.Domain.Events.Workplace;

public class WorkplaceCreatedEvent : DomainEvent
{
    public WorkplaceCreatedEvent(Entities.Workplace workplace)
    {
        Workplace = workplace;
    }

    public Entities.Workplace Workplace { get; }
}
