﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IWG.Heimdall.Domain.Entities;

namespace IWG.Heimdall.Domain.Events.Workplace;

public class WorkplaceUpdatedEvent : DomainEvent
{
    public WorkplaceUpdatedEvent(Entities.Workplace workplace)
    {
        Workplace = workplace;
    }

    public Entities.Workplace Workplace { get; }
}
