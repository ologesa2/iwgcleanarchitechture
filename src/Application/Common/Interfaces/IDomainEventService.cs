﻿using IWG.Heimdall.Domain.Common;

namespace IWG.Heimdall.Application.Common.Interfaces;

public interface IDomainEventService
{
    Task Publish(DomainEvent domainEvent);
}
