﻿using IWG.Heimdall.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace IWG.Heimdall.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<TodoList> TodoLists { get; }

    DbSet<TodoItem> TodoItems { get; }

    DbSet<Workplace> Workplaces { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}

