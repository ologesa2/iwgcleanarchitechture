﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Application.Workplaces.Queries.GetWorkplacesWithPagination;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace IWG.Heimdall.Application.Workplaces.Queries.GetWorkplaceByIdQuery;

public class GetWorkplaceByIdQuery :IRequest<WorkplaceBriefDto>
{
    public int WorkplaceIdentifier { get; set; }
}



public class GetWorkplaceWithPaginationQueryHandler : IRequestHandler<GetWorkplaceByIdQuery, WorkplaceBriefDto>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetWorkplaceWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

 

    public async Task<WorkplaceBriefDto> Handle(GetWorkplaceByIdQuery request, CancellationToken cancellationToken)
    {
        var workplace = await _context.Workplaces.FirstOrDefaultAsync(wp => wp.WorkplaceIdentifier == request.WorkplaceIdentifier);

        if (workplace == null)
        {
            return null;
        }
        var workplaceBriefDto = _mapper.Map<WorkplaceBriefDto>(workplace);
        return workplaceBriefDto;
              
    }
}