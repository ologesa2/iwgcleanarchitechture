﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Application.Common.Mappings;
using IWG.Heimdall.Application.Common.Models;
using MediatR;

namespace IWG.Heimdall.Application.Workplaces.Queries.GetWorkplacesWithPagination;


public class GetWorkplacesWithPaginationQuery : IRequest<PaginatedList<WorkplaceBriefDto>>
{
    public int PageNumber { get; set; } = 1;
    public int PageSize { get; set; } = 10;
}

public class GetWorkplaceWithPaginationQueryHandler : IRequestHandler<GetWorkplacesWithPaginationQuery, PaginatedList<WorkplaceBriefDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetWorkplaceWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<WorkplaceBriefDto>> Handle(GetWorkplacesWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await _context.Workplaces
            //.Where(x => x.ListId == request.ListId)
            //.OrderBy(x => x.Title)
            .ProjectTo<WorkplaceBriefDto>(_mapper.ConfigurationProvider)
            .PaginatedListAsync(request.PageNumber, request.PageSize);
    }
}
