﻿using IWG.Heimdall.Application.Common.Mappings;
using IWG.Heimdall.Domain.Entities;

namespace IWG.Heimdall.Application.Workplaces.Queries.GetWorkplacesWithPagination;

public class WorkplaceBriefDto : IMapFrom<Workplace>
{
    public int WorkplaceIdentifier { get; set; }
    public int WorkplaceExternalIdentifier { get; set; }
    public string CentreName { get; set; }

    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public int CountryID { get; set; }
}
