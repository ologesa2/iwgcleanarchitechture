﻿using IWG.Heimdall.Application.Workplaces.Commands.UpdateWorkplace;
using FluentValidation;

namespace IWG.Heimdall.Application.Workplaces.Commands.UpdateTodoItem;

public class UpdateWorkplaceCommandValidator : AbstractValidator<UpdateWorkplaceCommand>
{
    public UpdateWorkplaceCommandValidator()
    {
        RuleFor(v => v.CentreName)
            .MaximumLength(200)
            .NotEmpty();
    }
}
