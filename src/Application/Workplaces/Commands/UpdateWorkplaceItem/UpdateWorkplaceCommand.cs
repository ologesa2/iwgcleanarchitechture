﻿using IWG.Heimdall.Application.Common.Exceptions;
using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.Domain.Events.Workplace;
using MediatR;

namespace IWG.Heimdall.Application.Workplaces.Commands.UpdateWorkplace;

public class UpdateWorkplaceCommand : IRequest
{
    public int WorkplaceIdentifier { get; private set; }
    public int WorkplaceExternalIdentifier { get; set; }
    public string CentreName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public int CountryID { get; set; }

    public void SetWorkplaceIdentifier(int id)
    {
        WorkplaceIdentifier = id;
    }
}

public class UpdateWorkplaceCommandHandler : IRequestHandler<UpdateWorkplaceCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateWorkplaceCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateWorkplaceCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Workplaces
            .FindAsync(new object[] { request.WorkplaceIdentifier }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(Workplace), request.WorkplaceIdentifier);
        }

        entity.WorkplaceExternalIdentifier = request.WorkplaceExternalIdentifier;
        entity.CountryID = request.CountryID;
        entity.City = request.City;
        entity.State = request.State;
        entity.Zip = request.Zip;
        entity.CountryID = request.CountryID;
        entity.Address1 = request.Address1;
        entity.Address2 = request.Address2;
        entity.Address3 = request.Address3;
        entity.CentreName = request.CentreName;


        entity.DomainEvents.Add(new WorkplaceUpdatedEvent(entity));

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
