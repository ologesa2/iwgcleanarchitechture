﻿using IWG.Heimdall.Application.Common.Exceptions;
using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.Domain.Enums;
using MediatR;

namespace IWG.Heimdall.Application.Workplaces.Commands.UpdateWorkplaceDetail;

public class UpdateWorkplaceDetailCommand : IRequest
{
    public int Id { get; set; }

    //public int ListId { get; set; }

    public PriorityLevel Priority { get; set; }

    //public string? Note { get; set; }
}

public class UpdateWorkplaceDetailCommandHandler : IRequestHandler<UpdateWorkplaceDetailCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateWorkplaceDetailCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateWorkplaceDetailCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Workplaces
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(Workplace), request.Id);
        }

        entity.WorkplaceIdentifier = request.Id;
        //TODO: implement
        //entity.Priority = request.Priority;
        //entity.Note = request.Note;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
