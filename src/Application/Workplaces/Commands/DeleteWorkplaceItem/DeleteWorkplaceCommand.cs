﻿using IWG.Heimdall.Application.Common.Exceptions;
using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.Domain.Events;
using IWG.Heimdall.Domain.Events.Workplace;
using MediatR;

namespace IWG.Heimdall.Application.Workplaces.Commands.DeleteWorkplace;

public class DeleteWorkplaceCommand : IRequest
{
    public int WorkplaceIdentifier { get; set; }
}

public class DeleteWorkplaceCommandHandler : IRequestHandler<DeleteWorkplaceCommand>
{
    private readonly IApplicationDbContext _context;

    public DeleteWorkplaceCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(DeleteWorkplaceCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Workplaces
            .FindAsync(new object[] { request.WorkplaceIdentifier }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(Workplace), request.WorkplaceIdentifier);
        }

        _context.Workplaces.Remove(entity);

        entity.DomainEvents.Add(new WorkplaceDeletedEvent(entity));

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
