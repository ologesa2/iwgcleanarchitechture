﻿using IWG.Heimdall.Application.Common.Interfaces;
using IWG.Heimdall.Domain.Entities;
using IWG.Heimdall.Domain.Events;
using IWG.Heimdall.Domain.Events.Workplace;
using MediatR;

namespace IWG.Heimdall.Application.Workplaces.Commands.CreateWorkplace;

public class CreateWorkplaceCommand : IRequest<int>
{
    public int WorkplaceExternalIdentifier { get; set; }
    public string CentreName { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string Zip { get; set; }
    public int CountryID { get; set; }

}

public class CreateWorkplaceCommandHandler : IRequestHandler<CreateWorkplaceCommand, int>
{
    private readonly IApplicationDbContext _context;

    public CreateWorkplaceCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateWorkplaceCommand request, CancellationToken cancellationToken)
    {
        var entity = new Workplace
        {
            WorkplaceExternalIdentifier = request.WorkplaceExternalIdentifier,
            Address1 = request.Address1,
            Address2 = request.Address2,
            City = request.City,
            Address3 = request.State,
            State = request.Zip,
            Zip = request.Zip,
            CentreName = request.CentreName,
            CountryID = request.CountryID
        };

        entity.DomainEvents.Add(new WorkplaceCreatedEvent(entity));

        _context.Workplaces.Add(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return entity.WorkplaceIdentifier;
    }
}
