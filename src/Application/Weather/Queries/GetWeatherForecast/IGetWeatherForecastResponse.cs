﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;

public interface IGetWeatherForecastResponse
{
    public List<WeatherForecast> Temperatures { get; set; }
}
