﻿using MassTransit;

namespace IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;

internal class GetWeatherForecastConsumer : IConsumer<IGetWeatherForecastRequest>
{
    public async Task Consume(ConsumeContext<IGetWeatherForecastRequest> context)
    {
        await context.RespondAsync<IGetWeatherForecastResponse>(new
        {
            Temperatures = new List<WeatherForecast>
            {
                new WeatherForecast(new DateTime(2020, 10, 10), 12),
                new WeatherForecast(new DateTime(2020, 10, 11), 13),
                new WeatherForecast(new DateTime(2020, 10, 12), 11),
            }
        });
    }
}
