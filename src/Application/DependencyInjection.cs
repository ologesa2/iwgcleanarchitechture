﻿using System.Reflection;
using FluentValidation;
using IWG.Heimdall.Application.Weather.Queries.GetWeatherForecast;
using MassTransit;
using MassTransit.Mediator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MediatR;

namespace IWG.Heimdall.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAutoMapper(Assembly.GetExecutingAssembly());
        services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
        services.AddMassTransitServices(configuration);

        return services;
    }
    static IServiceCollection AddMassTransitServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddMassTransit(x =>
        {
            x.UsingAzureServiceBus((context, cfg) =>
            {
                cfg.Host(configuration.GetValue<string>("MassTransit:ConnectionString"));
            });
        });

        MassTransit.Mediator.IMediator mediator = Bus.Factory.CreateMediator(cfg =>
        {
        });

        RegisterConsumers(mediator);

        services.AddSingleton(mediator);

        services.AddMediatR(typeof(DependencyInjection).Assembly);

        return services;
    }

    static void RegisterConsumers(MassTransit.Mediator.IMediator mediator)
    {
        mediator.ConnectConsumer<GetWeatherForecastConsumer>();
    }
}

