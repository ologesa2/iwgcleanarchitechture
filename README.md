


IWG Readme

Setup Steps

# Prerequisites

VS2022 & .NET 6 & Docker

# Installation with Postgresql

To create Postgresql container run from cmd

    docker run --name heimdall-postgres  -p 5432:5432  -e POSTGRES_PASSWORD=my_password123 -d postgres

Uncomment the lines below from the appsetting.json

 

     "Database": {
        "Provider": "PostGreSql",
        "DefaultConnection": "User ID=postgres;Password=my_password123;Server=localhost;Port=5432;Database=CleanArchitecture;Integrated Security=true;Pooling=true;"
      },

  Run the application. Database migrations should be run successfully and example data should be inserted.

## Migrations
  If you want to add new migrations rather than initial create
  
  1- "Open Tools->Nuget Package Manager->Package Manager Console

  1- Change the default project  src\NpgsqlMigrations and run the command below from Package Manager Console

  3- `Add-Migration <YourMigrationName> -StartupProject WebUI -Args "--database__provider PostGreSql"` 

# Installation with Mssql

To create Mssql container run from cmd

    docker run --name heimdall-mssql  -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=my_password123"  -p 1433:1433  -d mcr.microsoft.com/mssql/server:2019-latest

Uncomment the lines below from the appsetting.json

  

    "Database": {
        "Provider": "SqlServer",
        "DefaultConnection": "Server=localhost,1433;Database=IWG.HeimdallDb.WebUI;User Id=sa;Password=my_password123; MultipleActiveResultSets=true;"
      },

  Run the application. Database migrations should be run successfully and example data should be inserted.


## Migrations

  If you want to add new migrations rather than initial create

  1- Open Tools->Nuget Package Manager->Package Manager Console
  
  2- Change the default project  src\SqlServerMigrations and run the command below from Package Manager Console

  3- `Add-Migration <YourMigrationName> -StartupProject WebUI -Args "--database__provider SqlServer"` 

### Api Endpoint
http://localhost:5000/api/index.html?url=/api/specification.json

### Page Endpoint
http://localhost:5000/workplace


### DataGrip 
Cross-platform IDE for all popular databases. Works with any relational DB in a smart way. You can download from [here](https://www.jetbrains.com/datagrip/)


